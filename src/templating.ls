
#lodash = require 'lodash'
#M = require('meteor-core')(lodash)
#require('meteor-htmljs')(M)
#require('meteor-html-tools')(M)
#require('meteor-blaze-tools')(M)
#require('meteor-spacebars-compiler')(M)

{EJSONKeyedMap, parseFormula, typecheckFormula, getColumn, rootCell, TypedSet, evaluateFormula, $$, set} = Objsheets

tinyModel =
  evaluateFamily: (qFamilyId) -> new FamilyId(qFamilyId).typedValues()
  getColumn: (columnId) -> getColumn(columnId)
  typecheckColumn: (columnId) -> getColumn(columnId).type

jade = require("jade")



class JadeCodeGen
  (@rootContext = '_root') ->
    if _.isString @rootContext
      @rootContext = new EJSONKeyedMap([['this', @rootContext]])
    @indent = 0
    @buf = []

  top: (block) ->
    @emit "phi = (a) -> evalFunc Blaze.getData!ref!, a"
    @emit "-> return" ~>
      @block block
    @buf.join "\n"

  block: (block, contextVars=@rootContext) ->
    for n in block.nodes
      if n instanceof jade.nodes.Text
        @emit @string n.val
      else if n instanceof jade.nodes.Tag
        if n.name in <[ for + ]> && n.block.nodes[0] instanceof jade.nodes.Text
          @for n.block.nodes[0].val, n.block.nodes[1 to], contextVars
        else
          if n.code? || n.attrs.length || n.block.nodes.length
            @emit "HTML.getTag(#{@tagname n.name}) do" ~>
              if n.code? then @block({nodes: [n.code]}, contextVars)
              @attrs(n.attrs, contextVars)
              @block(n.block, contextVars)
          else
            @emit "HTML.getTag(#{@tagname n.name})!"
      else if n instanceof jade.nodes.Code
        try
          a = @formula n.val, contextVars
          @emit "Blaze.View #{JSON.stringify n.val} -> (phi #{JSON.stringify a}).map (.toString!)"
        catch e
          console.error e.stack
          @emit "HTML.getTag('span') {class: 'err', 'data-tip': #{JSON.stringify e.toString!}}"
        if n.block? then @block n.block, contextVars
      else
        ...

  value: (node, contextVars) ->
    if node instanceof jade.nodes.Text
      @emit JSON.stringify node.val
    else if node instanceof jade.nodes.Code
      a = @formula node.val, contextVars
      @emit "(phi #{JSON.stringify a}).map (.toString!)"
      if node.block? then ...
    else
      ...

  for: (formulaText, nodes, contextVars) ->
    a = @formula formulaText, contextVars
    @emit "Blaze.Each do" ~>
      @emit "-> phi #{JSON.stringify a}"
      @emit "-> return" ~>
        @block({nodes},
               @update(contextVars, 'this', a.type))

  formula: (formulaText, contextVars) ->
    #if (mo = formulaText.match /^"(.*)"$/)?
    #  formulaText = mo[1]
    #parseFormula(contextVars.get('this') ? '_root', formulaText)
    parser = Objsheets.setupParserCommon("ENTRY_FORMULA", contextVars)

    parser.parse(formulaText)
      typecheckFormula(tinyModel, contextVars, ..)

  string: (s) ->
    # this should return a LiveScript-readable string (be wary of #'s)
    JSON.stringify s# .replace /^"|"$/g \'

  tagname: (s) ->
    HTML.getTag(s)  # throw an exception at this point if s is not valid in htmljs
    @string s

  attrs: (attrs, contextVars) ->
    if attrs.length > 0
      @emit "do" ~>
        for {name, val} in attrs
          @emit "#{JSON.stringify name}: ->" ~> @value val, contextVars

  emit: (code, f) ->
    @buf.push ("   " * @indent) + code
    if f?
      @indent += 1 ; f! ; @indent -= 1

  update: (contextVars, name, type) ->
    contextVars.shallowClone()
      ..set(name, type)


JadeCompiler =
  compile: (templateText, filename=">template<") ->
    p = new jade.Parser(templateText, filename)
    ast = p.parse()
    cg = new JadeCodeGen
    LiveScript.compile cg.top(ast), eval: true, header: false


_selected = new ReactiveVar(new TypedSet('text', set()))

evalFunc = (thisVal, a) ->
  try
    r = evaluateFormula($$.model, new EJSONKeyedMap([['this', thisVal], ['selected', _selected]]), a)
    r.elements!map ->
      value: it
      type: r.type
      ref: -> new TypedSet(@type, set([@value]))
      toString: -> @value.toString!
  catch e
    console.error "#{e} in #{a}"
    console.error e.stack
    [{ref: -> new TypedSet('text', set(["!"]))}]


# It would have been wise to use the Jade parser.
# Unforunately it is hard-coded there that all code nodes
# must be valid Javascript.
# So instead, we re-implement the required functionality.
MiniJade =

  blocker: (text) ->
    blk = (lines) ->
      i = 0 ; ln = -> l = lines[i] ; i := i + 1 ; l
      while (line = ln!)? && !(mo = line.match /^(\s*)\S/)? then ;
      if line?
        re = RegExp "^#{mo[1]}\\S"
        blk-i = [j for line,j in lines when line.match re]
        for j,i in blk-i
          root: lines[blk-i[i]] .slice mo[1].length
          subtrees: blk lines[blk-i[i]+1 til blk-i[i+1]]
      else
        []
    blk text.split '\n'

  TOK:    #   1    2    3    4    5    6   7    8    9
    WS:    /(\()|(\))|(\[)|(\])|(\{)|(\})|(")|(\\")|(\s)/
    COMMA: /(\()|(\))|(\[)|(\])|(\{)|(\})|(")|(\\")|(,)/
  # 1,3,5: open brackets
  # 2,4,6: close brackets
  # 7: open+close quotes
  # 8: escaped
  # 9: separator

  balancer: (text, tok, subparts=false, maxsplit=undefined) ->
    stack = [] ; q = [] ; state = 0 ; out = []
    buf = text
    i = j = k = 0
    enter = (j) -> if stack.length == 0 then q.push text.substr k, i - k ; j
    leave = (k) -> if stack.length == 0 then q.push text.substr k, i - k ; j
    if !subparts then [enter, leave] = [(->), (-> k)]
    while buf.length && (mo = buf.match tok)?
      i = j + mo.index ; j = i + mo.0.length
      if state == 0
        if mo.1? || mo.3? || mo.5? || mo.7? then stack.push enter j
        else if mo.2? || mo.4? || mo.6? then k = leave stack.pop!
        if mo.7? then state = 1
      else if state == 1
        if mo.7? then k = leave stack.pop! ; state = 0
      buf = buf.substr mo.index+mo.0.length
      if mo.9? && stack.length == 0
        out.push q ++ [text.substr k, i - k].filter ->it
        k = j ; q = []
        if out.length >= maxsplit then break

    while stack.length then k = leave stack.pop!
    out ++ [q ++ [text.substr k].filter ->it].filter ->it.length

  text: (blocks, pretext) ->
    collect = (t) -> [t.root].concat ...(t.subtrees.map collect)
    new jade.nodes.Block pretext
      for block in blocks
        for txt in collect block then ..push new jade.nodes.Text txt

  tag: (name, head, block) ->
    [name, ...cls] = name.split '.'
    new jade.nodes.Tag name, block
      for c in cls then ..setAttribute 'class' new jade.nodes.Text c
      for attr-blk in head[1 to] when attr-blk not in <[ = . ]>
        for [attr] in @balancer attr-blk, @TOK.COMMA
          if (mo = attr.match /^([^=]+)=(.*)$/)?
            ..setAttribute mo.1, new jade.nodes.Code mo.2

  parser: (blocks, pretext) ->
    new jade.nodes.Block pretext
      for block in blocks
        [head,tail] = @balancer block.root, @TOK.WS, true, 1
        [_,name,mod] = if head.length == 1 then head.0.match /^(.+?)([.=])?$/
                       else ['',head[0], head[*-1]]
        tail = tail?.0 ? ''
        if name in <[ = | ]>
          S = if name == '=' then jade.nodes.Code else jade.nodes.Text
          ..push do
            new S tail
              if block.subtrees.length then ..block = @parser block.subtrees
        else
          S = if mod == '=' then jade.nodes.Code else jade.nodes.Text
          sub = if mod == '.' then @~text else @~parser
          pretail = if tail? then new S tail
          ..push @tag name, head, sub block.subtrees, pretail


MiniJadeCompiler =
  compile: (templateText, filename=">template<") ->
    p = new jade.Parser(templateText, filename)
    ast = MiniJade.parser MiniJade.blocker templateText
    cg = new JadeCodeGen  new EJSONKeyedMap([['this', '_root'], ['selected', 'text']])
    LiveScript.compile cg.top(ast), eval: true, header: false


sp = ': {{#each phi "Gauge.Level"}} <dq>a {{phi "`*`"}}</dq> {{/each}} :'
ja = """
style.
  table.pl { border-collapse: collapse; }
  .pl td { border: 1px solid black; }
table.pl
  + Gauge.Level
    tr
      td= `*`
      td= Level = Gauge.reading
p
  = Gauge.reading.`*`
"""

ja = """
p
  + Gauge
    b Hi.
  """

class Designer

  (@src) ->
    @t = new ReactiveVar(null)
    @err = new ReactiveVar(null)
    Tracker.autorun ~>
      try
        #M.SpacebarsCompiler.compile(src.get(), {isTemplate:true})
        MiniJadeCompiler.compile @src.get()
        #JadeCompiler.compile src.get()      
          @t.set new Blaze.Template( eval( .. ) )
        @err.set(null)
      catch e
        console.log e.stack
        @err.set(e)

      if @t.get!?
        @t.get!rendered = ->
          sel = @$('select').val!
          _selected.set(new TypedSet('text', set([sel])))
        @t.get!events do
          "change select": (ev, t) ->
            sel = t.$('select').val!
            _selected.set(new TypedSet('text', set([sel])))

  bind-divs: (w, editor, preview) ->
    view = null
    Tracker.autorun ~>
      if view? then Blaze.remove view ; view := null
      if @t.get!?
        preview.off!  # remove all events so that inline scripts can install their own
        view := Blaze.renderWithData(@t.get!, rootCell, preview[0])

    Tracker.autorun ~>
      if @err.get! then preview.add-class 'err' else preview.remove-class 'err'
      preview.attr 'data-tip' @err.get!?stack

    editor = w.CodeMirror editor[0], do
      mode: "text/x-jade"
      lineNumbers: true
      matchBrackets: true

    editor
      ..set-value @src.get!
      ..on \change ~> @src.set editor.get-value!

    @editor = editor


class Boilerplate

  call: (transaction, argsObj, callback?) ->
    args = @glue(argsObj)
    console.log args
    $$.call("executeCannedTransaction", transaction, args, Objsheets.standardServerCallbackThen(callback))

  glue: (argsObj) ->
    value = (v) ->
      if v instanceof Element
        Blaze.getData(v).ref!
      else
        if $.isArray(v) then v else [v]  # TODO: detect type?
    {}
      for k,v of argsObj
        ..[k] = value(v)


open-designer = (template-filename) ->
  src = new ReactiveVar(ja)

  filename = path.join projdir, template-filename
  if fs.existsSync filename
    src.set fs.readFileSync(filename, 'utf-8')

  Tracker.autorun ->
    fs.writeFileSync filename, src.get(), 'utf-8'

  w = window.open("./designer.html", "templating")
  g = gui.Window.get(w)
  g.once 'loaded' !->
    designer = $(w.document).find('#designer')
    editor = designer.find('#editor')
    preview = designer.find('#preview')
    w.$ = -> $ it, w.document
    w.$.on = -> preview.on ...&
    w.$$ = new Boilerplate
    g.designer = new Designer(src)
      ..bind-divs w, editor, preview


@ <<< {open-designer}
