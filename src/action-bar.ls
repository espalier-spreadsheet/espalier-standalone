require! fs
require! path

{$$} = Objsheets


class ActionBarC
  fullTextToShow: {set: ->}
  isLoading: {set: ->}
  changeColumnArgs: new ReactiveVar(null)
  isExpanded: -> false
  hasUnsavedData: -> false

  init: (div) ->
    top = $ '<p>' .append-to div
    @type = $ '<select>' .append-to top
      ..change ~>
        args = @changeColumnArgs.get()?.0
        assert -> args != null
        $$.call 'changeColumnSpecifiedType', args.columnId, @type.val!
    @formula = $ '<input>' .append-to div
      ..keydown (event) ~>
        if event.which == 13  # ENTER
          args = @changeColumnArgs.get()?.0
          if args
            col = $$.model.getColumn(args.columnId)
            value = ..val!
            formula = if value == "" then null else Objsheets.parseFormula col.parent, value
            Objsheets.$$.call 'changeColumnFormula', col._id, formula
    
    @switch-view = $ '#ActionBar #switch-view'
      ..change !->
        v = ..val!
        if v == "" then v = null
        else if v == "*" then v = prompt("View identifier:") ? window.viewId.get!
        if v then ..val v
        window.viewId.set v
    @open-spreadsheet = $ '#ActionBar #open-spreadsheet'
      ..change !->
        v = ..val!
        SpreadsheetData.load v
        window.viewId.set null

  update: ->
    # Fills in list of types (depends on columnId for 'auto' option)
    # Select current column
    args = @changeColumnArgs.get()?.0
    if args? && args.columnId?
      col = $$.model.getColumn(args.columnId)
      if col.formula?
        @formula.val Objsheets.stringifyFormula(col.parent, col.formula)
      else
        @formula.val ''
      @update-types col._id
      if col.isObject then @type.val '' .prop 'disabled' true
      else @type.val col.type .prop 'disabled' false
    else
      @update-types Objsheets.rootColumnId
      @formula.val ''
      @type.val '' .prop 'disabled' true

  update-types: (columnId) ->
    # FIXME: 'auto (<inferred type>)' feature of type menu does not work
    types = Template.changeColumn._helpers.typeMenu.call({columnId})
    @type.empty!
      for item in types.items
        if item instanceof Objsheets.HtmlOption
            $ '<option>' .val item.value .text item.label .append-to ..
        if item instanceof Objsheets.HtmlOptgroup
          $ '<optgroup>' .attr 'label', item.label .append-to ..
            for option in item.members
              $ '<option>' .val option.value .text option.label .append-to ..

  update-views: ->
    @switch-view
      all-ids = Objsheets.Views.find!map (._id)
      if (current = window.viewId.get!) not in [null, ...all-ids]
        all-ids.push current
      ..empty!
      for v in [""] ++ all-ids
        ..append <| $ '<option>' .val v .text v
      ..append (item-new = $('<option>').attr('id', 'new').val("*").text("New..."))
      .val current

  update-spreadsheets: ->
    dirs = ["lib/objsheets/private/dump", "private/data"]
    names = {}
      for dir in dirs
        for fn in fs.readdirSync(dir)
          if (mo = /^(.*)_columns.json$/.exec fn)?
            ..[mo.1] = path.join(dir, mo.1)
    @open-spreadsheet
      ..empty!
      ..append <| $ '<option>' .val '' .text "Open..."
      for let name, path of names
        ..append <| $ '<option>' .val path .text name
      ..val ''


ActionBar = new ActionBarC()

$ ->
  if (bar = $ '#ActionBar').0
    ActionBar.init bar
    Tracker.autorun -> ActionBar.update!
    Tracker.autorun -> ActionBar.update-views!
    ActionBar.update-spreadsheets!


StatusLine = new ReactiveVar("")

$ ->
  status-bar = $ '<div>' .add-class 'status'
  $ '#ActionBar' .append status-bar
  Tracker.autorun -> status-bar .text StatusLine.get()



Objsheets <<< {ActionBar}

export ActionBar, StatusLine