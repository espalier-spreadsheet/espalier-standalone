fs = require('fs')
path = require('path')

{Tablespace, Model, FormulaEngine, CellHandle} = Objsheets


Objsheets.clientTablespace = Objsheets.ServerTablespace.instances['standalone'] =
  id: 'standalone'
  Columns: new LocalCollection("columns")
  Families: new LocalCollection("families")
  Views: new LocalCollection("views")
  Procedures: new LocalCollection("procedures")
  oldCellsCollection: new LocalCollection("oldcells")
  getCollection: -> {columns: @Columns, families: @Families, views: @Views, procedures: @Procedures}[it]
  call: (name, ...args) -> Meteor.call(name, @.id, ...args)
  run: (f) -> if f? then @model.runDeferringEvaluation ~> f.apply(@, [])
  runTransaction: (f) -> @run(f)
  subscribeAll: ->

LocalCollection.prototype.dropIfExists = -> this.remove({})

Objsheets.ServerTablespace.getCurrent = Objsheets.ClientTablespace.getCurrent = Objsheets.Tablespace.getCurrent

{Columns, Families, Views, $$, $$server} = Objsheets

removeEJSONPrefix = ->
  if _.isArray(it)
    [removeEJSONPrefix(..) for it]
  else if _.isObject(it)
    {}
      for k,v of it
        #console.log k,v
        if (mo = /^EJSON(.*)$/.exec(k)) then k = mo[1]
        ..[k] = removeEJSONPrefix(v)
  else
    it

doc-ejson = -> EJSON.fromJSONValue(removeEJSONPrefix(it))

fromFile = (collection, filename, clear=true) ->
  if clear then collection.remove({})
  text = fs.readFileSync filename, 'utf-8'
  for doc in JSON.parse text
    collection.insert doc-ejson(doc)

toFile = (collection, filename) ->
  text = EJSON.stringify collection.find!fetch!
  fs.writeFileSync filename, text

SpreadsheetData =
  load: (spreadsheet-path) !->
    @path = localStorage['spreadsheet-path'] = spreadsheet-path
    Objsheets.clientTablespace.id = path.basename(spreadsheet-path)
    Objsheets.ServerTablespace.instances[Objsheets.clientTablespace.id] = Objsheets.clientTablespace
    try
      fromFile Columns, "#{spreadsheet-path}_columns.json"
      try
        # backward-support old cells collection if exists
        fromFile $$server.oldCellsCollection, "#{spreadsheet-path}_cells.json"
      catch e
        fromFile Families, "#{spreadsheet-path}_families.json"
      try
        fromFile Views, "#{spreadsheet-path}_views.json"
      catch e
        # ok no views is fine
    catch e
      console.error e
      console.warn "Spreadsheet '#{spreadsheet-path}' does not exist, starting blank."
    
    @_init-model!

  save: (spreadsheet ? @path) !->
    toFile Columns, "#{spreadsheet}_columns.json"
    toFile Families, "#{spreadsheet}_families.json"
    toFile Views, "#{spreadsheet}_views.json"

  _init-model: ->
    model = new Model
    $$.model?destroy!
    $$.model = $$.modelForData = model
    model.runDeferringEvaluation = (f) ->
      @startDeferringEvaluation!
      #                                             # /* async equivalent of: */
      Promise.resolve!                              # try
      .then f                                       #   f
      .finally ~> new Promise (resolve, reject) ~>  # finally
        <~ requestAnimationFrame                    #   @performDeferredEvaluation!
        @performDeferredEvaluation!
        resolve!
      
    model.typecheckAll!
    setTimeout ~>        # deferred to after loading the procedures == yuck
      if model.deferringEvaluation then model.performDeferredEvaluation!
    , 0


spreadsheet = localStorage['spreadsheet-path'] ? "lib/objsheets/private/dump/milk"
#spreadsheet = "private/data/twit"
#spreadsheet = "private/data/rss"

if (! spreadsheet is /^\//)
  spreadsheet = path.join projdir, spreadsheet

/*
 * Monkey-patch Views.upsert to correctly handle id-only selectors.
 * This seems to be supported by Collection, but not by LocalCollection.
 */
Views._upsert = Views.upsert
Views.upsert = (selector, values) ->
  if _.isString(selector) then selector = {_id: selector}
  @_upsert selector, values

/*
 * This part demonstrates extending the formula environment
 * with custom functions.
 */

wrapper = (signature, f) ->
  wrapped = ->
    f ...([..set.elements! for &])
  wrapped <<< {signature}

match_ = wrapper {ret: 'bool'} ([re], [str]) ->
  [(new RegExp(re).exec(str))?]

if (FormulaEngine.external?)
  FormulaEngine.external <<< {match: match_}



Meteor.startup ->
  Objsheets.rootCellHandle = new CellHandle(Objsheets.rootQCellId)

Meteor.startup ->
  $$.formulaEngine = new FormulaEngine

  SpreadsheetData.load spreadsheet

  @ <<< {SpreadsheetData}
