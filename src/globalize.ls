process.browser = 'nwjs'   # this makes handsontable not use 'require' thinking that this is a NodeJS environment.

require 'datejs' ; @Date = global.Date  # better way?

for own k,v of Package
  @ <<< v

@Jison =
  Parsers:
    _register: (k,v) -> @[k] = v

Meteor._methods = {}
Meteor.methods = (o) -> @_methods <<< o
Meteor.call = (name, ...args) ->
  callback = if args[*-1]?.call? then args.pop! else (->)
  try
    @_methods[name](...args)
      if .. instanceof Promise
        ..then (callback null, _) .catch (callback)
      else
        callback null, ..
  catch e
    callback e
Meteor.connection = {}
Meteor.publish = ->

Meteor.makeErrorType = (name, ctor) ->  /* monkey-patch a bug in Meteor's error code */
  errorClass = ->
    if Error.captureStackTrace
      Error.captureStackTrace @, errorClass
    else
      @stack = new Error .stack

    ctor.apply @, &       /* <-- Meteor's version does not return ctor's result */
      ..errorType = name

  Meteor._inherits errorClass, Error
  errorClass

@Router = {route: ->}
@Iron = {Location: {onClick: ->}}
@Template =
  ObjsheetsApp: {helpers: ->, events: ->}
  Spreadsheet: {helpers: ->, events: ->}
  actionBar: {helpers: ->, events: ->}
  formulaBands: {helpers: ->, events: ->}
  changeColumn: {helpers: (@_helpers) ->, events: ->}
  globalMacros: {helpers: ->, events: ->}
  html_select_content: {helpers: ->}
  registerHelper: ->

@tx =
  Transactions: {remove: ->, find: ->}
  setTransactionsCollection: ->
  collectionIndex: {}
  start: ->
  purgeRedo: ->
  rollback: ->

@Mongo =
  Collection: LocalCollection

@DDP =
  onReconnect: ->
