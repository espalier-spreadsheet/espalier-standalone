{ClientView, View, ViewCell, StateEdit, CellHandle, FamilyHandle, assert} = Objsheets



{CellContextMenu, $$} = Objsheets


class SpreadsheetView extends Objsheets.SpreadsheetView

  onSelectionChange: ->
    super ...
    cell = @selected[0]
    StatusLine.set "#{cell?.columnId ? cell?.qCellId?.columnId ? ""} #{cell?.kind ? ""} #{cell?.addColumnId ? ""} #{cell?.objectExtent ? ""}"
    
  isPending: -> $$.model.deferringEvaluation



bind-keys = (view) ->
  $(document).keydown (ev) ->
    if (ev.target.tagName != 'INPUT') then view.onKeyDown(ev)
  $(document).keypress (ev) ->
    if (ev.target.tagName != 'INPUT') then view.onKeyPress(ev)


view = null

rebuildTable = (viewId) ->
  $ '#View'
    ..add-class "handsontable pal-alternating showTypes"

  if !view?
    view := new SpreadsheetView($$.model)
    bind-keys view

  view.build new View(viewId)

  #console.log(view.grid);

  sel = view.selectedCoords
  if view.table then view.refreshTable!
  else view.createTable! .append-to '#View'
  view.selectedCoords = sel
  window <<< {view}


export rebuildTable
