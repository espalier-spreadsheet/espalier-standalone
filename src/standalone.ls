{guarded, View, ClientView, rebuildView, $$} = Objsheets



standardServerCallback = (error, result) ->
  if (error) then
    console.error error.stack  # some errors may not require logging. but how to tell?
    alert error

standardServerCallbackThen = (callback) ->
  (error, result) ->
    standardServerCallback(error, result)
    if (typeof callback is "function")
      callback(error, result);

Objsheets <<< {standardServerCallback, standardServerCallbackThen}


popup = (value="") ->
  popup.rv =
    accept: (cb) -> Object.observe(@, ~> cb(@value))
  $ '#popup' .css display: 'block', left: 200, top: 50
    ..find 'input' .val(value) .focus()
  popup.rv

$ ->
  $ '#popup'
    ..focusout ->
      delete popup.rv
      ..css display: 'none'
    ..keydown (ev) ->
      if ev.which == 13  # Enter
        popup.rv.value = ..find 'input' .val!
        ..focusout()
      else if ev.which == 27  # Esc
        ..focusout()
      ev.stopPropagation()


viewId = new ReactiveVar(null)
window <<< {viewId}


start-time = new Date()

$ ->
  Meteor.startup ->
    Objsheets.DEBUG_REVERSE_ENGINEERING = false
    Objsheets.ClientAppHelpers.openCallback!        # this is usually called by ClientTablespace.open()
    Objsheets.ServerAppHelpers.loadProcedures $$.id # this is usually called by
                                                    # ServerTablespace.internalEnsureInitialized()
    if $$.model.deferringEvaluation
      $$.model.performDeferredEvaluation!
    console.log "%cfinished computing; #{new Date() - start-time}ms", "color: #69f"

  guarded = (op) -> -> try op! catch e then console.error(e)

  timing = (msg, op) ->
    s = new Date()
    op!
      .. ; elapsed = new Date() - s; setTimeout (-> console.log "%c#{msg}; #{elapsed}ms", "color: #99f"), 100

  if $ '#View' .0
    $ '#View'
      ..add-class "handsontable pal-alternating showTypes"
    Meteor.startup ->
      Tracker.autorun ->
        #rebuildView viewId.get!
        timing "rebuilt view", -> rebuildTable viewId.get!

  # Mavo HTML templating
  mavo = void
  $ '#open-designer' .click !->
    mavo := window.open("../apps/#{$$.id}/index.html", "templating")
    window.mavo = mavo

  Mavo.Functions.urlOption = -> /* always return undefined, this function seems to be problematic in NWjs */

  $(window).resize ->
    $('.ht_master .wtHolder').outerHeight("100%")
    $('.ht_master .wtHolder').outerWidth("100%")

  Meteor.startup ->
    window <<<< Objsheets
    window <<<< {Objsheets.$$, Objsheets.Columns, Objsheets.Cells, Objsheets.Views, Objsheets.Procedures}

  #
  # RSS feed demo
  #
  nullIfFail = -> try it! catch then null

  Meteor.startup ->
    if (feed-fh = nullIfFail(-> rootCellHandle.childFamilyByName("Feed")))
      reload-rss = ->
        for let feed-ch in feed-fh.cells!
          for url in feed-ch.childFamilyByName("url").values!
            $.get url
            .done (data) ->
              $$.run ->
                items =
                  for item in $(data).find('item')
                    pubDate:     $(item).find('pubDate').text!
                    title:       $(item).find('title').text!
                    description: $(item).find('description').text!
                    link:        $(item).find('link').text!
                ServerAppHelpers.writeObj do
                  qCellId: feed-ch
                  Item: items
            .fail (err) -> console.error err

      Tracker.autorun reload-rss

      window <<< {reload-rss}

  #
  # Some sample procedures (for twit demo)
  #
  /*
  Objsheets.RelsheetsServer.procedures 'standalone', do
    'hi':
      params: [["name", "text"]]
      body: """
      let u = new $User
      u.name := name
      """
    'post':
      params: [["who", "User"], ["what", "text"]]
      body: """
      let m = new who.Tweet
      m.when := d"now"
      m.what := what
      """
    'unpost':
      params: [["it", "User:Tweet"]]
      body: "delete it"

  Tracker.autorun ->
    errCount = 0
    Objsheets.RelsheetsServer.compile('standalone', (-> errCount += 1))
    console.log "Compiled procedures (#errCount errors)"
*/

export popup, rebuildView
