

function getStats() {
    // Gets all formulas as strings
    // (except completely trivial ones such as "true")
    formulas = Columns.find({formula: {$ne: null}}).fetch()
      .filter((c) => !isCompletelyTrivial(c.formula))
      .map((x) => {
        let text = stringifyFormula(x.parent, x.formula);
        return {text: text, nnodes: countASTNodes(x.formula), ntokens: countTokens(text)}; });

    // Get all procedures as {name, params, body}
    procedures = Procedures.find().fetch().map(stringifyProcedure);

    // Count lines (1 line accounts for procedure header)
    locs = sum(procedures.map((x) => 1 + x.body.split(/\s*\n\s*/).filter((s) => s.length > 0).length));

    for (f of formulas) {
        console.log(`[${f.ntokens} tokens]  ${f.text}`);
    }
    let tot = sum(formulas.map((f) => f.ntokens));
    console.log(`total: ${tot}, average: ${tot / formulas.length}`);

    return {formulas: formulas.length, locs: locs};
}

function sum(l) { return l.reduce((x,y) => x + y, 0); } /* is this really not defined somewhere? */

function isCompletelyTrivial(formula) {
    return formula[0] == "lit";
}

function countASTNodes(formula) {
    if (formula && formula.map) return 1 + sum(formula.map(countASTNodes));
    else return 1;
}

function countTokens(formulaText) {
    let tok = /\w+|[.,|&{}()$:!=+-]+|["].*?["]/g;
    let i = 0;
    while (tok.exec(formulaText)) i++;
    return i;
}
