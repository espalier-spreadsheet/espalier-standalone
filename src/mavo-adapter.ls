
Mavo.Backend.register(opener.Mavo.Backend.Objsheets)

get-search = -> encodeURI(decodeURI(window.location.search.substr(1)))

fetch-template = (filename, template-name, replace={}) ->
  fs = require('fs')
  templText = fs.readFileSync(filename, 'utf-8')
  h = $.parseHTML(templText)
  el = h.filter (-> it.getAttribute?('name') == template-name) .0
  if el?
    console.log "template '#{template-name}': #{bare-line-count(el)} lines"
    window.el = el
    el.innerHTML.replace /\{\{(.*?)\}\}/g (, expr) ->
      replace[expr] ? get-search!
  else
    console.warn "did not find template '#{template-name}'"


render-template = (filename, template-name, replace={}) ->
  console.log("rendering")
  if (html = fetch-template ...)
    document.body.innerHTML = html
  console.log("done")


# Let's hope there are no <!-- --> comments in the styles
bare-line-count = -> line-count(strip-comments(it.innerHTML)) - line-count-in($(it.content).find('style'))

line-count-in = -> it.map((i,x)->line-count(x.outerHTML)).get!reduce((+), 0)


line-count = -> it.split(/\s*\n\s*/).filter (.length>0) .length

strip-comments = (html) -> html.replace(/<!--[\s\S]*?-->/g, '')

export render-template, fetch-template
