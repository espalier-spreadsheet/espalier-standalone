#!/bin/bash

# Use Git submodules instead?
git clone git@bitbucket.org:objsheets/objsheets lib/objsheets
( cd lib/objsheets ; git checkout technion )

npm install
bower install
node node_modules/nwjs-kremlin/reload.js
