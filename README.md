Espalier (formerly Object Spreadsheets) - "Standalone"

Takes advantage of NW.js [http://nwjs.io] technology to run the data model, formula engine,
and UI in a single process. Meteor reactive collections are used to get reactive UI
functionality, but Meteor's client-server framework is not instantiated.

There are quite a few dependencies. The current version works with the Technion branch of Espalier
[https://bitbucket.org/espalier-spreadsheet/espalier/branch/technion]. NPM and Bower are needed to install dependencies, and
NW.js is required to run the application (can be installed through `npm i -g nw`).
Run `setup.sh` after checkout to set up a working directory.
